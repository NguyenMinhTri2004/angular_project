import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { DetailComponent } from './detail/detail.component';

const routes: Routes = [
  { path: 'about', component: DetailComponent },
];

@NgModule({ 
  imports: [RouterModule.forRoot(routes)], 
  exports: [RouterModule] }) 


export class AppRoutingModule { }
